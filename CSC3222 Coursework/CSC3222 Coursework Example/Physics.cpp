#include "Physics.h"
#include "..\..\nclgl\common.h"
#include "../../nclgl/window.h"
#include <algorithm>

Entity temp; // I should consider changing the leader to be a raider, so base - raider - leader - follower
Entity arrayTemp;
Leader lTemp;
bool raiderSpawn = false; 
bool endGame = false;

float arrowFiredTime = 100000000000000.0f; // this is set high because it will be overwritten when space is pressed, if the player wait until this time then they will break the game. But they should die by then
float restriction = -6000.0f; // To ensure the first set of arrows will always fire 

bool dragonToLeader = true;

int numRaider;
//Vector3 tempRaiderVel;
Vector3 tempRaiderPos;

bool pathFinding = false;

bool fire = false;

bool leaderAlive = true;

int impulseDead = 3;

Vector3 currentPath;
int pathIndex = 0;
int numRaiderToPathFind;

Physics::Physics()
{
	numRaiders = 16;
	//raiderState.resize(numRaiders);

	for (int i = 0; i < numRaiders - 1; i++) // minus one because numraiders includes the leader and we dont want leader to be in the array
	{
		temp.physicsNode.setPosition(Vector3(27 + 27 * i, -325.0f, -199.9f));

		if (i >= 6) {
			temp.physicsNode.setPosition(Vector3(-150 + 27 * i, -350.0f, -199.9f));
		}

		temp.physicsNode.setRotation(i*20.0);
		temp.physicsNode.setScale(Vector3(0, 0, 0));
		temp.setHitpoint(rand() % 150 + 100);

		raiders.push_back(temp);
		arrows.push_back(arrayTemp); 
	}

	//leader
	lTemp.physicsNode.setPosition(Vector3(-311 + 27 * 15, -350.0f, -199.9f));
	lTemp.physicsNode.setRotation(15 * 20.0);
	lTemp.physicsNode.setScale(Vector3(0, 0, 0));
	raiders.push_back(lTemp);


	for (int i = 0; i < numRaiders; i++) {
		raiders[i].setRadius(12.0f);
		raiders[i].physicsNode.setMass(1);
	}

	map.physicsNode.setPosition(Vector3(0.0f, 100.0f, -200.0f));
	map.physicsNode.setRotation(0.0f);
	map.physicsNode.setScale(Vector3(864.0f, 540.0f, 100.0f));

	dragon.physicsNode.setPosition(Vector3(500.0f, 90.0f, -199.9f));
	dragon.physicsNode.setRotation(0.0f);
	dragon.physicsNode.setScale(Vector3(50.0f, 50.0f, 1.0f));
	dragon.setRadius(50); // how big the dragon is 
	dragon.setHitpoint(1000);
	dragonState = 1;
	dragon.physicsNode.setMass(10);

	dragon.updateAggroRange(25);

	breath.physicsNode.setPosition(Vector3(0.0f, -50.0f, 6.3f));
	breath.physicsNode.setRotation(270.0f);
	breath.physicsNode.setScale(Vector3(2.0f, 1.0f, 1.0f));

	breathState = 1;

	// regional nodes for path planning
	for (int i = 0; i < 21; i++) {
		node *temp = new node;
		temp->nodeID = i + 1;
		nodes.push_back(*temp);
	}

	nodes[0].position = Vector3(134.528, -358.614, 0);
	nodes[0].connectedNodes = { 2,6 };

	nodes[1].position = Vector3(12.641, -200, 0);
	nodes[1].connectedNodes = { 6,4,3,1 };

	nodes[2].position = Vector3(-258.412, -304.817, 0);
	nodes[2].connectedNodes = { 2,4,5 };

	nodes[3].position = Vector3(-193.059, -122.011, 0);
	nodes[3].connectedNodes = { 2,3,5,8,7,6 };

	nodes[4].position = Vector3(-425.103, -94.324, 0);
	nodes[4].connectedNodes = { 4,8,9,10,3 };

	nodes[5].position = Vector3(48.287, -57.204, 0);
	nodes[5].connectedNodes = { 1,2,4,7,15,17,18 };

	nodes[6].position = Vector3(-64.044, 78.817, 0);
	nodes[6].connectedNodes = { 14,8,4,6,15 };

	nodes[7].position = Vector3(-247.242, 89.329, 0);
	nodes[7].connectedNodes = { 14,7,4,5,9 };

	nodes[8].position = Vector3(-425.612, 130.883, 0);
	nodes[8].connectedNodes = { 11,10,5,8,14 };

	nodes[9].position = Vector3(-549.769, 54.176, 0);
	nodes[9].connectedNodes = { 11,9,5 };

	nodes[10].position = Vector3(-412.394, 282.227, 0);
	nodes[10].connectedNodes = { 12,14,9,10 };

	nodes[11].position = Vector3(-205.685, 338.093, 0);
	nodes[11].connectedNodes = { 13,11 };

	nodes[12].position = Vector3(-143.433, 470.255, 0);
	nodes[12].connectedNodes = { 12 };

	nodes[13].position = Vector3(-147.592, 180.493, 0);
	nodes[13].connectedNodes = { 11,9,8,7,15 };

	nodes[14].position = Vector3(115.237, 213.67, 0);
	nodes[14].connectedNodes = { 14,16,17,7,6 };

	nodes[15].position = Vector3(296.923, 315.247, 0);
	nodes[15].connectedNodes = { 15,17,18,19,20 };

	nodes[16].position = Vector3(266.343, 96.89, 0);
	nodes[16].connectedNodes = { 6,15,16,18 };

	nodes[17].position = Vector3(449.278, 74.760, 0);
	nodes[17].connectedNodes = { 17,6,16,20,21 };

	nodes[18].position = Vector3(545.659, 264.746, 0);
	nodes[18].connectedNodes = { 20,16 };

	nodes[19].position = Vector3(570.739, 166.297, 0);
	nodes[19].connectedNodes = { 16,19,18,21 };

	nodes[20].position = Vector3(574.209, 54.018, 0);
	nodes[20].connectedNodes = { 18,20 };

	// Cavewalls, snowman barriers.
	for (int i = 0; i < 4; i++) {  
		switch (i) {
		case 0:
			temp.physicsNode.setPosition(Vector3(220.0f, -100.0f, -199.9f));
			temp.physicsNode.setRotation(0.0f);
			temp.physicsNode.setScale(Vector3(90, 90, 0));
			temp.setRadius(90);
			temp.physicsNode.setMass(1000000000000000000);
			circles.push_back(temp);
			break;
		case 1:
			for (int i = 0;i < 3; i++) {
				temp.physicsNode.setPosition(Vector3(400.0f + i * 200.0f, -200.0f, -199.9f));
				temp.physicsNode.setRotation(0.0f);
				temp.physicsNode.setScale(Vector3(185, 185, 0));
				temp.setRadius(185);
				temp.physicsNode.setMass(1000000000000000000);
				circles.push_back(temp);
			}
			break;
		case 2:
			for (int i = 0;i < 4; i++) {
				temp.physicsNode.setPosition(Vector3(800.0f + i * -13, -200.0f + i * 200, -199.9f));
				temp.physicsNode.setRotation(0.0f);
				temp.physicsNode.setScale(Vector3(185, 185, 0));
				temp.setRadius(185);
				temp.physicsNode.setMass(1000000000000000000);
				circles.push_back(temp);
			}
			break;
		case 3:
			for (int i = 0;i < 7; i++) {
				temp.physicsNode.setPosition(Vector3(700.0f + i * -100, 500.0f, -199.9f));
				temp.physicsNode.setRotation(0.0f);
				temp.physicsNode.setScale(Vector3(100, 100, 0));
				temp.setRadius(100);
				temp.physicsNode.setMass(1000000000000000000);
				circles.push_back(temp);
			}
			break;
		}
	}

	for (int i = 0; i < 5; i++) {
		temp.physicsNode.setPosition(Vector3(20 + i * -50, 400.0f + i * -30, -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(50, 50, 0));
		temp.setRadius(50);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 5; i++) {
		temp.physicsNode.setPosition(Vector3(-180 + i * -40, 280.0f, -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(50 + i * -10, 50 + i* -10, 0));
		temp.setRadius(50 + i * -10);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 7; i++) {
		temp.physicsNode.setPosition(Vector3(0 + i * -50, 620.0f, -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(100 + i * -8, 100 + i * -8, 0));
		temp.setRadius(100 + i * -8);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 5; i++) {
		temp.physicsNode.setPosition(Vector3(-380 + i * -70, 620.0f + i * -80, -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(100  , 100 , 0));
		temp.setRadius(100);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 3; i++) {
		temp.physicsNode.setPosition(Vector3(-390 + i * 50, 380.0f , -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(70 + i * -20, 70 + i * -20, 0));
		temp.setRadius(70 + i * -20);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 3; i++) {
		temp.physicsNode.setPosition(Vector3(-390 + i * 50, 380.0f, -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(70 + i * -20, 70 + i * -20, 0));
		temp.setRadius(70 + i * -20);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 6; i++) {

		if (i == 3) {
			continue;
		}

		temp.physicsNode.setPosition(Vector3(-660 + i * -10, 300.0f + i * -100, -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(100, 100, 0));
		temp.setRadius(100);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 4; i++) {

		temp.physicsNode.setPosition(Vector3(-710 + i * 150, -200.0f + i * -100 , -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(100, 100, 0));
		temp.setRadius(100);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	for (int i = 0; i < 4; i++) {

		temp.physicsNode.setPosition(Vector3(-300 + i * 70, -400.0f, -199.9f));
		temp.physicsNode.setRotation(0.0f);
		temp.physicsNode.setScale(Vector3(30 + i * 20 , 30 + i * 20, 0));
		temp.setRadius(30 + i * 20);
		temp.physicsNode.setMass(1000000000000000000);
		circles.push_back(temp);
	}

	temp.physicsNode.setPosition(Vector3(-285, -320, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(39, 39, 0));
	temp.setRadius(39);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-173, -280, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(17, 17, 0));
	temp.setRadius(17);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-92, 250, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(42, 42, 0));
	temp.setRadius(42);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(95, 350, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(42, 42, 0));
	temp.setRadius(42);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);

	/*temp.physicsNode.setPosition(Vector3(145, 41, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(0, 0, 0));
	temp.setRadius(0);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);*/

	temp.physicsNode.setPosition(Vector3(490, 362, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(29, 29, 0));
	temp.setRadius(29);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-579, -78, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(47, 47, 0));
	temp.setRadius(47);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-546, 200, -199.9f));
	temp.physicsNode.setRotation(0.0f);
	temp.physicsNode.setScale(Vector3(41, 41, 0));
	temp.setRadius(41);
	temp.physicsNode.setMass(1000000000000000000);
	circles.push_back(temp);

// ---------------------------rubble-----------------------//
	temp.physicsNode.setPosition(Vector3(-449, -170, -199.9f));
	temp.physicsNode.setScale(Vector3(35, 35, 0));
	temp.setRadius(35);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-451, -70, -199.9f));
	temp.physicsNode.setScale(Vector3(51, 51, 0));
	temp.setRadius(51);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-286, 50, -199.9f));
	temp.physicsNode.setScale(Vector3(75, 75, 0));
	temp.setRadius(75);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-243, 115, -199.9f));
	temp.physicsNode.setScale(Vector3(52, 52, 0));
	temp.setRadius(52);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-162, 176, -199.9f));
	temp.physicsNode.setScale(Vector3(61, 61, 0));
	temp.setRadius(61);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-4, 119, -199.9f));
	temp.physicsNode.setScale(Vector3(49, 49, 0));
	temp.setRadius(49);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(6, 246, -199.9f));
	temp.physicsNode.setScale(Vector3(49, 49, 0));
	temp.setRadius(49);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(79, 235, -199.9f));
	temp.physicsNode.setScale(Vector3(43, 43, 0));
	temp.setRadius(43);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(131, 172, -199.9f));
	temp.physicsNode.setScale(Vector3(38, 38, 0));
	temp.setRadius(38);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(190, 216, -199.9f));
	temp.physicsNode.setScale(Vector3(46, 46, 0));
	temp.setRadius(46);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(263, 294, -199.9f));
	temp.physicsNode.setScale(Vector3(81, 81, 0));
	temp.setRadius(81);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(318, 337, -199.9f));
	temp.physicsNode.setScale(Vector3(78, 78, 0));
	temp.setRadius(78);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(553, 255, -199.9f));
	temp.physicsNode.setScale(Vector3(54, 54, 0));
	temp.setRadius(54);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(569, 180, -199.9f));
	temp.physicsNode.setScale(Vector3(51, 51, 0));
	temp.setRadius(51);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(568, 77, -199.9f));
	temp.physicsNode.setScale(Vector3(43, 43, 0));
	temp.setRadius(43);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(541, 4, -199.9f));
	temp.physicsNode.setScale(Vector3(23, 23, 0));
	temp.setRadius(23);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-286, 497, -199.9f));
	temp.physicsNode.setScale(Vector3(40, 40, 0));
	temp.setRadius(40);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(-237, 534, -199.9f));
	temp.physicsNode.setScale(Vector3(31, 31, 0));
	temp.setRadius(31);
	rubbles.push_back(temp);

	temp.physicsNode.setPosition(Vector3(265, 92, -199.9f));
	temp.physicsNode.setScale(Vector3(45, 45, 0));
	temp.setRadius(45);
	rubbles.push_back(temp);

	// -- pond -- //

	pond.physicsNode.setPosition(Vector3(449.278, 74.760, 0));
	pond.physicsNode.setScale(Vector3(60, 60, 0));
	pond.setRadius(60);

	// -- treasure -- //

	treasure.physicsNode.setPosition(Vector3(449.278, 74.760, 0));
	treasure.physicsNode.setScale(Vector3(60, 60, 0));
	treasure.setRadius(60);

}

Physics::~Physics()
{
	raiders.clear();
	arrows.clear();
}

Vector3 beforeCollision; // a global variable to store previous collision for raiders, pretty bad


void Physics::UpdatePhysics(float msec, float msecStart)
{
	//Astar();

	if (endGame == false) {

		float dt = msec * 0.001;
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_RIGHT) && raiderSpawn) {

			raiders[15].physicsNode.getRotation() -= 1;


		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_LEFT) && raiderSpawn) {

			raiders[15].physicsNode.getRotation() += 1;


		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_DOWN) && raiderSpawn) {

			bool collision = false;
			float y, x;

			if (RubbleDetection(raiders[15])) {
			 y = sin((raiders[15].physicsNode.getRotation() + 30.0f)*PI / 180.0f) * 10.8f * dt;
			 x = cos((raiders[15].physicsNode.getRotation() + 30.0f)*PI / 180.0f) * 10.8f * dt;
			}
			else {
			y = sin((raiders[15].physicsNode.getRotation() + 30.0f)*PI / 180.0f) * 27.0f * dt;
			x = cos((raiders[15].physicsNode.getRotation() + 30.0f)*PI / 180.0f) * 27.0f * dt;
			}

			for (int c = 0; c < circles.size(); c++) {

				// I'll probably have to do another thing here for collision with the dragon

				if (CollisionDetection(raiders[15], circles[c])) {
					raiders[15].physicsNode.setPosition(beforeCollision);
					collision = true;
					break;
				}
			}
			if (!collision) {
				beforeCollision = raiders[15].physicsNode.getPosition();
				raiders[15].physicsNode.getPosition() += Vector3(x, y, 0.0f);
			}
			//}
		}
		
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_UP) && raiderSpawn) {
			
			bool collision = false;
			float y, x;

			if (RubbleDetection(raiders[15])) {
				y = sin((raiders[15].physicsNode.getRotation() - 150.0f)*PI / 180.0f) * 10.8f * dt;
				x = cos((raiders[15].physicsNode.getRotation() - 150.0f)*PI / 180.0f) * 10.8f * dt;
			}
			else {
				y = sin((raiders[15].physicsNode.getRotation() - 150.0f)*PI / 180.0f) * 27.0f * dt;
				x = cos((raiders[15].physicsNode.getRotation() - 150.0f)*PI / 180.0f) * 27.0f * dt;
			}

			for (int c = 0; c < circles.size(); c++) {

				if (CollisionDetection(raiders[15], circles[c])) {
					raiders[15].physicsNode.setPosition(beforeCollision);
					collision = true;
					break;
				}
			}
			if (!collision) {
				beforeCollision = raiders[15].physicsNode.getPosition();
				
				raiders[15].physicsNode.getPosition() += Vector3(x, y, 0.0f);
			}
		}
		
		//spawning raiders
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_K) && raiderSpawn == false) {

			raiderSpawn = true;

			for (int i = 0; i < numRaiders; i++) {
				raiders[i].physicsNode.setScale(Vector3(10.0f, 10.0f, 1.0f));
				//lTemp.physicsNode.setScale(Vector3(10.0f, 10.0f, 1.0f));
			}
		}

	
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_J)) {

			 numRaiderToPathFind = rand() % 14 + 0;

			//tempRaiderPos = Astar(raiders[numRaider].physicsNode.getPosition()); // the astar function should affect the velocity continuesly, perhaps move all the define value stuff outside of the class and it only consists of movements
			AstarToGold(raiders[numRaider].physicsNode.getPosition());
			pathFinding = true;

		}
		

		// firing arrows
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_SPACE) && raiderSpawn == true) {

			arrowFiredTime = msecStart;

			if ((msecStart - restriction) > 6000.0f) { //  6 seconds delay

				//  the leader and he doesnt fire arrows so its -1
				for (int i = 0; i < raiders.size()-1; i++) {

					// only spawn arrows if the raiders is not dead
					if (raiders[i].getHitpoint() > 0) {

						arrows[i].physicsNode.setPosition(Vector3(raiders[i].physicsNode.getPosition()));

						arrows[i].physicsNode.setScale(Vector3(20.0f, 20.0f, 1.0f));

						arrows[i].setRadius(50);
					}

					//direction
					Vector3 dir = dragon.physicsNode.getPosition() - arrows[i].physicsNode.getPosition(); // the reason why this wasnt working was because this line is not in updatephysics , so the velocity is not being updated. Instead of having a
					dir.Normalise();
					arrows[i].physicsNode.setVelocity(dir * 81 * dt);

					//rotation to face the dragon
					Vector3 dis = dragon.physicsNode.getPosition() - arrows[i].physicsNode.getPosition();
					float angle_radians = atan2f(dis.y, dis.x);
					float degrees = (angle_radians * 180) / PI;
					arrows[i].physicsNode.setRotation(degrees + 220.0f);



				}

				restriction = msecStart; // if arrow fired then we set the time of when it fired

			}

		}

		// arrow collision with the dragon and cave walls, despawning after 2 seconds.
		for (int i = 0; i < arrows.size(); i++) { 

			if (CollisionDetection(dragon, arrows[i])) {
				// giving players the allusion that the arrow is gone, but its actually somewhere else. Since getting rid of them and recompiling the array
				// will take too long and waste processing time
				arrows[i].physicsNode.setPosition(Vector3(2000, 0, 0));
				arrows[i].setRadius(0);
				arrows[i].physicsNode.setScale(Vector3(0, 0, 0));
				dragon.setHitpoint(dragon.getHitpoint() - 10);
			}

			// arrows despawn after 2 seconds, dosent need to be a for loop for every arrow since they all fire at the same time
			if (msecStart - arrowFiredTime > 2000) {
				arrows[i].physicsNode.setPosition(Vector3(2000, 0, 0));
				arrows[i].setRadius(0);
				arrows[i].physicsNode.setScale(Vector3(0, 0, 0));
			}

			// arrow collision with cavewalls
			for (int c = 0; c < circles.size(); c++) {

				if (CollisionDetection(circles[c], arrows[i])) {
					arrows[i].physicsNode.setPosition(Vector3(2000, 0, 0));
					arrows[i].setRadius(0);
					arrows[i].physicsNode.setScale(Vector3(0, 0, 0));
				}
			}

			
		}



		float shift;

		shift = 0.3*msec;

		shift = 0.1*msec;

		if (fire) {
			if (breath.physicsNode.getScale().x > 100.0f)
			{
				breathState = 0;
				breath.physicsNode.getScale().x = 100;
				breath.physicsNode.getScale().y = 50;
			}
			else if (breath.physicsNode.getScale().x < 2.0f)
			{
				breathState = 1;
				breath.physicsNode.getScale().x = 2.0f;
				breath.physicsNode.getScale().y = 1.0f;
			}

			if (breathState == 1)
			{
				breath.physicsNode.getScale().x += 1.5*shift;
				breath.physicsNode.getScale().y += 0.75*shift;
			}
			else
			{
				breath.physicsNode.getScale().x -= 2 * shift;
				breath.physicsNode.getScale().y -= shift;
			}
		}


		// Victory only if an arrow takes the dragon to 0 hitpoints
		if (dragon.getHitpoint() <= 0) {
			congratulations.physicsNode.setPosition(Vector3(0.0f, 100.0f, -199.0f));
			congratulations.physicsNode.setRotation(0.0f);
			congratulations.physicsNode.setScale(Vector3(864.0f, 540.0f, 100.0f));

			endGame = true;
		}

		// checking if all the raider's health
		bool healthGone = true;
		for (int i = 0; i < raiders.size(); i++) {

			if (!raiders[i].getHitpoint() == 0) {
				healthGone = false;
			}

		}
		if (healthGone) {
			gameover.physicsNode.setPosition(Vector3(0.0f, 100.0f, -199.0f));
			gameover.physicsNode.setRotation(0.0f);
			gameover.physicsNode.setScale(Vector3(864.0f, 540.0f, 100.0f));

			endGame = true;
		}


		if (raiderSpawn == true) {

			//dragonToLeader = false; // disable this to move the dragon again

			Vector3 dragonDir;
			// dragon going towards the leader
			if (dragonToLeader == true) {
			dragonDir = raiders[15].physicsNode.getPosition() - dragon.physicsNode.getPosition();
			dragonDir.Normalise();
			// dragon's rotation
			Vector3 dis = raiders[15].physicsNode.getPosition() - dragon.physicsNode.getPosition();
			float angle_radians = atan2f(dis.y, dis.x);
			float degrees = (angle_radians * 180) / PI;
			dragon.physicsNode.setRotation(degrees + 100);
			}

			
			for (int i = 0; i < numRaiders; i++) {

				// increase hit point
				if (CollisionDetection(raiders[i], treasure)) {

				}

				// increase health
				if (CollisionDetection(raiders[i], pond)) {

				}

				// each raiders are following/moving towards the leader and facing the leader
				float speed = 27;

				if (leaderAlive) {

					// facing the dragon
					Vector3 dis = dragon.physicsNode.getPosition() - raiders.at(i).physicsNode.getPosition();
					float angle_radians = atan2f(dis.y, dis.x);
					float degrees = (angle_radians * 180) / PI;

					if (i != 15) {
						raiders.at(i).physicsNode.setRotation(degrees + 150.f);
					}
					// the direction
					Vector3 dir = raiders[15].physicsNode.getPosition() - raiders[i].physicsNode.getPosition();
					dir.Normalise();

					if (i != 15) {

						if (RubbleDetection(raiders[i])) {
							speed = 10.8;
						}

						raiders[i].physicsNode.setVelocity(dir * speed * dt);
					}

				// the leader is dead
				} else {

					// facing the entrance
					Vector3 dis = Vector3(65, -317, 199.99) - raiders.at(i).physicsNode.getPosition();
					float angle_radians = atan2f(dis.y, dis.x);
					float degrees = (angle_radians * 180) / PI;
					if (i != 15) {
						raiders.at(i).physicsNode.setRotation(degrees + 150.f);
					}

					// going towrads teh entrance
					Vector3 dir = Vector3(65,-317,199.99) - raiders[i].physicsNode.getPosition();
					dir.Normalise();

					if (i != 15) {

						if (RubbleDetection(raiders[i])) {
							speed = 10.8;
						}

						raiders[i].physicsNode.setVelocity(dir * speed * dt);
					}

					if (raiders[i].physicsNode.getPosition().y < -300) {
						raiders[i].setHitpoint(0);
					}
				}

				if (CollisionDetection(raiders[i], dragon)) {
					if (i != 15) {
						CollisionResponse(dragon, raiders[i]);
					}
				}

				// for checking if a raider is in range
				float radiusSum = raiders[i].getRadius() + dragon.getRadius() + 108;
				float distSq = (raiders[i].physicsNode.getPosition() - dragon.physicsNode.getPosition()).LengthSq();
				
				// stopping the breathing unless it dectects someone near by as the lines below will re activate it
					//fire = false;
				
				if ((radiusSum * radiusSum) >= (distSq + 108) && raiders[i].getHitpoint() != 0) {
					
					fire = true;
					dragonToLeader = false;
					//moving to leader, if in range of 20 feet moves towars them instead
					dragonDir = raiders[i].physicsNode.getPosition() - dragon.physicsNode.getPosition();
					//Vector3 dirLeader = raiders[15].physicsNode.getPosition() - dragon.physicsNode.getPosition();
					dragonDir.Normalise();

					// angle to the new raider
					Vector3 dis = raiders[i].physicsNode.getPosition() - dragon.physicsNode.getPosition();
					float angle_radians = atan2f(dis.y, dis.x);
					float degrees = (angle_radians * 180) / PI;
					dragon.physicsNode.setRotation(degrees+90);

					// calculating if a raider is within the angle of the dragon
					if (abs((degrees - dragon.physicsNode.getRotation() +90 )) <= 30) {
						
						// raiders take damage
							raiders[i].setHitpoint(raiders[i].getHitpoint() - (50 * dt));
						
						if (raiders[i].getHitpoint() <= 0) {
							raiders[i].physicsNode.setScale(Vector3(0, 0, 0));
							raiders[i].setRadius(0);

						}

						if (raiders[15].getHitpoint() <= 0) {
							leaderAlive = false;
						}


					} 
					
				} else {
					
					dragonToLeader = true;
		
				}
			
				// raider collisions with circle
				for (int c = 0; c < circles.size(); c++) {

					//collision betwen dragon and cavewalls
					if (CollisionDetection(circles[c], dragon)) { 

						CollisionResponse(circles[c], dragon);

					}

					//collision betwen raiders and dragon
					if (CollisionDetection(circles[c], raiders[i]) && i != 15) { 

						CollisionResponse(circles[c], raiders[i]);
					} else {

						// flocking
						for (int v = 1; v < numRaiders; v++) { // every raider except the leader

							Vector3 raiderPos1 = raiders[i].physicsNode.getPosition();
							Vector3 raiderPos2 = raiders[v].physicsNode.getPosition();
							Vector3 distDiff2 = raiderPos1 - raiderPos2;

							float myDist2 = distDiff2.Length();

							float radii2 = raiders[i].getRadius() + raiders[v].getRadius();

							if (myDist2 < radii2 && leaderAlive) { // if (myDist2 < radii2 && radiusSum * radiusSum <= distSq) {
								Vector3 overlap = distDiff2;
								overlap.Normalise();

								overlap = overlap * (radii2 - myDist2);

								if (i == 15) {
									raiders[v].physicsNode.getPosition() -= overlap;

								}
								else if (v == 15) {
									raiders[i].physicsNode.getPosition() += overlap;
								}
								else {
									raiders[i].physicsNode.getPosition() += overlap;
									raiders[v].physicsNode.getPosition() -= overlap;
								}

							}

							// leader is dead so we push all the characters out
							if (myDist2 < radii2 && !leaderAlive) {

								Vector3 overlap = distDiff2;
								overlap.Normalise();

								overlap = overlap * (radii2 - myDist2);

								if (i == 15) {
									raiders[v].physicsNode.getPosition() -= overlap * impulseDead;

								}
								else if (v == 15) {
									raiders[i].physicsNode.getPosition() += overlap * impulseDead;
								}
								else {
									raiders[i].physicsNode.getPosition() += overlap * impulseDead;
									raiders[v].physicsNode.getPosition() -= overlap * impulseDead;
								}

							}

							if (impulseDead <= 1) {
								impulseDead -= 1;
							}

						}
					}
			   
					
				
			}
				
		}

	

			// velocity for dragon
			dragon.physicsNode.setVelocity(dragonDir * 16.2 * dt);
			// a boolean for a path finding is enable

			
			if (raiders[numRaiderToPathFind].physicsNode.getPosition() == currentPath && pathIndex < 10) {
				pathIndex += 1;
			}

			if (pathFinding) { // should read from the closelist starting from the newest by TRACKING EACH PARENT NODE until you reach destionation, as it include dead end nodes

					currentPath = closeList[pathIndex].position;
					Vector3 newVelocity = currentPath - raiders[numRaider].physicsNode.getPosition();
					newVelocity.Normalise();
					raiders[numRaider].physicsNode.setVelocity(newVelocity);
				
			}
			
			for (int i = 0; i < arrows.size(); i++) { // a loop to set velocity for arrows

				arrows[i].physicsNode.setVelocity(arrows[i].physicsNode.getVelocity());

			}

	
			


		}

	} else {

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_X)) {
			// if endGame is true then wait for the player to press x then end the game
		}	
}

		
}

void Physics::CollisionResponse(Entity &a, Entity &b)
{

	Vector3 VAB;

	Vector3 normal;

	normal.z = 0;

	if (b.physicsNode.getMass() > 5) {

		 VAB = a.physicsNode.getVelocity() - b.physicsNode.getVelocity();

		 normal = a.physicsNode.getPosition() - b.physicsNode.getPosition();
	} else {

		VAB = b.physicsNode.getVelocity() - a.physicsNode.getVelocity();

		normal = b.physicsNode.getPosition() - a.physicsNode.getPosition();
	}

	normal.z = 0;

	normal.Normalise(); // to get the angle after collision

	float normalx2 = (normal.x * normal.x) + (normal.y * normal.y);

	float collisionNormal = (VAB.x * normal.x) + (VAB.y * normal.y); // relative veloctiy(VAB, collision speed) from A to B along the collision normal direction(dot product, collision direction)

	// impulse scalar with restitution
	float j = -(1 + 1) * collisionNormal; // why does 1 make everything so fast????????????
	j /= normalx2 * (1 / b.physicsNode.getMass()) + (1 / a.physicsNode.getMass());

	b.physicsNode.setVelocity(b.physicsNode.getVelocity() - normal * (j / b.physicsNode.getMass()));
	a.physicsNode.setVelocity(a.physicsNode.getVelocity() + normal * (j / a.physicsNode.getMass()));
}

bool Physics::CollisionDetection(Entity & a, Entity & b)
{
	float radiusSum = a.getRadius() + b.getRadius();
	float distSq = (a.physicsNode.getPosition() - b.physicsNode.getPosition()).LengthSq();

	if (radiusSum * radiusSum > distSq) {
		return true;
	}
	else {
		return false;
	}
}

bool Physics::RubbleDetection(Entity &a) {

	for (int i = 0; i < rubbles.size(); i++) {

		if (CollisionDetection(rubbles[i], a)) {
			return true;
		}
	}

	return false;

}

void Physics::AstarToGold(Vector3 startPos) // returns a velocity for the raider to 
{
	node Gold = nodes[12];

	// simply going to the nearest node to start route planning
	float lowest = (startPos - nodes[0].position).LengthSq();
	int nodeIndex = 0;
	for (int i = 1; i < nodes.size(); i++) {

		float dist = (startPos - nodes[i].position).LengthSq();

		if (dist < lowest) {

			lowest = dist;
			nodeIndex = i;
			
		}

	}

	// assigning the startnode to a global variable
	startNode = nodes[nodeIndex];

	// if the nearest/first node is our destination
	openList.push_back(startNode);

	fOpen();
	ConnectedNodes(startNode);
	
	while (!destinationReach) {
		fOpen();
		ConnectedNodes(closeList[closeList.size()-1]);
	}
	
}

heuristic Physics::heurCal(node &startNode, node &q, node &dest)
{
	heuristic temp;

	temp.g = (q.position- startNode.position ).LengthSq();
	temp.h = (q.position - dest.position).LengthSq();
	temp.f = temp.g + temp.h;

	return temp;
}

bool compareByF(const node &a, const node &b)
{
	return a.cost.f < b.cost.f;
}

// 5,a,b,c
void Physics::fOpen() {

	node Gold = nodes[12];

	//int lowestFIndexNode = 0;

	//int lowestF = openList[0].cost.f; // would not work if a path cannot be found

	std::sort(openList.begin(), openList.end(), compareByF);

	for (int i = 0; i < openList.size(); i++) {

		// something to stop the infinite loop at the moment
		if (closeList.size() == 10) {
			destinationReach = true;
		}

		if (openList[i].nodeID == Gold.nodeID) {
			closeList.push_back(openList[i]);
			destinationReach = true;

			// go to step 8, a function for tracking back

			break;
			// it recommends people to first remove the object from openlist, but you found the destination
			// anyway so you dont have to do it
		} 

	}

	if (!destinationReach) {

		// add p to closeList
		//for (int i =0; i < openList.size();
		closeList.push_back(openList[0]);
		
		// remove from openList
		openList.erase(openList.begin());
	}
}

// 6,a,b,c,d
void Physics::ConnectedNodes(node p) {

	// should not be here if you want this to move somewhere else as well
	node Gold = nodes[12];
	// look for all connected
	for (int i = 0; i < p.connectedNodes.size(); i++) {

		for (int v = 0; v < nodes.size(); v++) {

			// can confirm that the if statment can indeed get the nodes connected to it!!!!
			if (p.connectedNodes[i] == nodes[v].nodeID) { // getting nodes that are connected to p by comparing their ID

				nodes[v].cost = heurCal(startNode, nodes[v], Gold);
				nodes[v].parentNodeID = p.nodeID;

				if (p.connectedNodes[i] != p.parentNodeID) {
					openList.push_back(nodes[v]);
				}
				// cehck for Q's presence on the open or closed list.................. dont do this yet
				bool qinopen = false;
				//bool qinclose = false;

				for (int k = 0; k < openList.size(); k++) {

					if (nodes[v].nodeID == openList[k].nodeID) {
						qinopen = true;
					}
				}

				/*for (int k = 0; k < closeList.size(); k++) {

					if (nodes[v].nodeID == closeList[k].nodeID) {
						qinclose = true;
					}

				}*/

				if (qinopen == true) {

					// if the new g-value of Q is lower than the old g-values..... WHAT OLD G VALUE?
				}



			}
		}
	}

}

