#pragma once
//#include "Vector3.h" 
#include "../../nclgl/Vector3.h"

class PhysicsNode {

public:

	//PhysicsNode();
	//~PhysicsNode();

	Vector3& getPosition() {return position;}
	const Vector3& getPosition() const { return position; }

	float& getRotation() {return rotation;}// get values that has been changed
	const float& getRotation() const { return rotation; }// get values without changing it
	
	Vector3& getScale() { return scale;}
	const Vector3& getScale() const { return scale; }

	Vector3& getVelocity() { return velocity; }
	const Vector3& getVelocity() const { return velocity; }

	float& getMass() { return mass; }
	const float& getMass() const { return mass; }
	
	void setPosition(Vector3 v) {
		this->position = v;
	}
	
	void setRotation(float x) {
		this->rotation = x;
	}

	void setScale(Vector3 v) {
		this->scale = v;
	}

	void setVelocity(Vector3 v) {
		this->velocity = v;
		position += velocity;
	}

	void setVelocityWithMsec(Vector3 v, float msec) {
		this->velocity = v;
		position += velocity*msec;
	}

	void setMass(float v) {
		this->mass = v;
	}

private:
	
	Vector3 position;
	float rotation;
	Vector3 scale;
	Vector3 velocity;
	float mass;
	
};