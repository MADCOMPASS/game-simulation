#pragma once

#include <iostream>
#include "PhysicsNode.h"
//#include "PhysicsNode"

class Entity {

public:
	PhysicsNode physicsNode; // entity access physicsnode, then physicsnode access physics data

	void updateAggroRange(float x) {
		aggroRange = x;
	}

	float getAggroRange() { return aggroRange; }

	float getRadius() {
		return radius;
	}

	void setRadius(float i) {
		radius = i;
	}

	int getHitpoint() {
		return hitpoint;
	}

	void setHitpoint(int i) {
		hitpoint = i;
	}

protected:
	float aggroRange;
	float x;
	float y;
	float radius;
	int hitpoint; // make it int for equality test, speically for 0 equality
	

};