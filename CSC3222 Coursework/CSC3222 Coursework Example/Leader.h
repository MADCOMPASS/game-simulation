#pragma once

#include <iostream>
#include "PhysicsNode.h"
#include "Entity.h"

class Leader : public Entity {

public:

	Leader();

	int getfollowersInRange() {
		return followersInRange;
	}

protected:
	int followersInRange;
	
	
};