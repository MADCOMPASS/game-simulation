#include "../../nclgl/window.h"
#include "Renderer.h"
#include "Physics.h"

#pragma comment(lib, "nclgl.lib")

int main() {
	Window w("CSC3222 Coursework", 1920, 1080, false);
	if (!w.HasInitialised()) {
		return -1;
	}

	Physics physics;
	Renderer renderer(w, &physics);
	
	if (!renderer.HasInitialised()) {
		return -1;
	}

	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_X)) {
		float msec = w.GetTimer()->GetTimedMS(); // just a note that If I use the GetMS function then everything will just get bigger and bigger, as this returns the gap betfore hte last GetTimedMS
		float msecStart = w.GetTimer()->GetMS();

		physics.UpdatePhysics(msec, msecStart);
		renderer.UpdateScene(msec);
		renderer.RenderScene();
		
	}

	

	return 0;
}