#pragma once

#include <iostream>
#include "PhysicsNode.h"
#include "Entity.h"

class Dragon: public Entity {

public:
	int getEntitiesInRange() { return entitiesInRange; }

private:
	int entitiesInRange;
};