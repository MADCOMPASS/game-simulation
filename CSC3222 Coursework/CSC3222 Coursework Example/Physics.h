#pragma once

#include <vector>
#include "../../nclgl/Vector3.h"
#include "PhysicsNode.h"
#include "Entity.h"
#include "Dragon.h"
#include "Leader.h"
#include "Follower.h"

using std::vector;

/* An elementary Physics Data structure, containing the bare minimum of information necessary to
say where an object should appear, and which way it should point. As we increase the complexity
of our physics system, we will add elements to this structure, but the elements which can affect
the graphical representation of entities are present below (and integrated with the graphics
framework already, to make your life easier). Ultimately, you may decide to replace this data
structure entirely (for example with a class to allow inclusion of embedded functions) but be
aware that doing so might require some slight doctoring of Renderer::Renderer() and
SceneNode::Update() */

struct PhysicsData {
	Vector3 position;
	float rotation;
	Vector3 scale;
};

struct heuristic {
	int g;
	int h;
	int f;
};

struct node {
	Vector3 position; 
	int	nodeID;
	vector<int> connectedNodes; // an array of node IDs identifying which nodes are connected to the considered node
	//vector<int> connectedNodesCost; // cost associated with reaching those nodes, should be the same size as above and same index
	heuristic cost;
	int parentNodeID;
};



/* Our placeholder physics system includes an update function, and physics data for each entity
in our environment. We also include a single integer to inform the dragon's behaviour and breath
weapon scaling.*/

class Physics {
public:
	Physics();
	~Physics();

	void UpdatePhysics(float msec, float msecStart);
	void CollisionResponse(Entity &a, Entity &b);
	bool CollisionDetection(Entity &a, Entity &b);
	bool RubbleDetection(Entity &a);
	void AstarToGold(Vector3 startPos);
	heuristic heurCal(node &p, node &q, node &dest);
	void fOpen();
	void ConnectedNodes(node p);

	Entity map;
	Dragon dragon;
	Entity breath;
	vector<Entity> arrows;
	vector<Entity> raiders;
	Leader leader;

	node startNode;
	vector<node> nodes;
	vector<node> openList;
	vector<node> closeList;


	Entity congratulations;
	Entity gameover;
	//Entity circle; 
	vector<Entity> circles; // immovable objects
	vector<Entity> rubbles; 
	Entity treasure;
	Entity pond;
	
	// something for A* to keep the while loop active
	bool destinationReach = false;

private:
	int numRaiders;
	int	dragonState;
	int breathState;
	std::vector<int> raiderState;
};